var Promise = require("bluebird");
var Article = Promise.promisifyAll(require("../wherever/the/article/model/is"));

var whichService = function whichService(input) {
    // Keep in mind that you need to *return* the promise (not just the values in the callback!) if you want to use them as return-like values.
    isDupe(input).then(function(data) {
        console.log( data);
    })
}

var isDupe = function isDupe(input) {
    return Promise.try(function(){
        return Article.findOneAsync({ params: input.params, taskSubType: input.taskSubType ,taskType: input.taskType});
    }).then(function(result){
        return true;
    }).catch(function(err){
        return false;
    });
};